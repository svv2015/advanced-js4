const filmsListElement = document.getElementById('filmsList');

const charactersListElement = document.getElementById('charactersList');

function fetchData(url) {
  return fetch(url)
    .then(response => response.json())
    .catch(error => console.error('Error:', error));
}
function showCharacters(characters) {
    charactersListElement.innerHTML = '';
  
    characters.forEach(character => {
      const characterElement = document.createElement('div');
      characterElement.textContent = character.name;
      charactersListElement.appendChild(characterElement);
    });
  }
  
  fetchData('https://ajax.test-danit.com/api/swapi/films')
    .then(data => {
      data.forEach(film => {
        const filmElement = document.createElement('li');
        filmElement.innerHTML = `<strong>Episode ${film.episodeId}:</strong> ${film.name} - ${film.openingCrawl}`;
        filmsListElement.appendChild(filmElement);
        fetchData(film.characters)
        .then(characters => {
          filmElement.addEventListener('click', () => {
            showCharacters(characters);
          });
        });
    });
  })
  .catch(error => console.error('Error:', error));